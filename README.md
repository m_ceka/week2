# week1

## Lesson November 14th, 2022

1. Available projects: [pdf](./slides/Projects_available.pdf)
2. numpy library: [notebook](./scripts/numpy_library.ipynb) and [pdf](./slides/library_numpy.pdf)

## Lesson November 16th, 2022

1. Works with strings: [notebook](./scripts/working_with_strings.ipynb) and [pdf](./slides/working_with_strings.pdf)
1. Regular Expression: [notebook](./scripts/regular_expression.ipynb) and [pdf](./slides/regular_expression.pdf)
2. Introduction to Natural Language Processing
3. Cleaning texts
4. seaborn library: [notebook](./scripts/seaborn_visualization_parta.ipynb) and [notebook](./scripts/seaborn_visualization_partb.ipynb)

Application of previous libraries to a set of use cases:

5. pollutants data exploration: [notebook](./use_cases/air_quality_data_visualization.ipynb) and [pdf](./scripts/air_quality_data_visualization.pdf)
6. california houses price data exploration: [notebook](./use_cases/california_housing_price_data_exploration.ipynb) and [pdf](slides/california_housing_price_data_exploration.pdf)

## Lesson November 17th, 2022

